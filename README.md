# NetModule WLAN Layer

![badge](https://badgen.net/badge/meta-netmodule-wlan/deprecated/red)

This instance of the Meta NM WLAN layer is **deprecated**

This project has been moved to [bitbucket](https://bitbucket.gad.local/projects/NM-NSP/repos/meta-netmodule-wlan/browse).
