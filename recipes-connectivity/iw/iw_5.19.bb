SUMMARY = "nl80211 based CLI configuration utility for wireless devices"
DESCRIPTION = "iw is a new nl80211 based CLI configuration utility for \
wireless devices. It supports almost all new drivers that have been added \
to the kernel recently. "
HOMEPAGE = "http://wireless.kernel.org/en/users/Documentation/iw"
SECTION = "base"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING;md5=878618a5c4af25e9b93ef0be1a93f774"

DEPENDS = "libnl-tiny"

SRC_URI = "http://www.kernel.org/pub/software/network/iw/${BP}.tar.xz \
           file://0001-Remove-obsolete-library.patch \
           file://001-nl80211_h_sync.patch \
           file://120-antenna_gain.patch \
           file://130-survey-bss-rx-time.patch \
           file://200-reduce_size.patch \
           file://400-wlan-srd-channel.patch \
           file://401-wlan-indoor-channel.patch \
           file://402-show-ant-gain.patch \
           file://403-clockbootime.patch \
           "

SRC_URI[sha256sum] = "f167bbe947dd53bb9ebc0c1dcef5db6ad73ac1d6084f2c6f9376c5c360cc4d4e"

inherit pkgconfig

CFLAGS += "-I${STAGING_DIR_TARGET}/usr/include/libnl-tiny -lnl-tiny -DCONFIG_LIBNL20 -D_GNU_SOURCE"
LDFLAGS += " -Wl,--gc-sections"

CFLAGS_remove += "-lnl-genl"

EXTRA_OEMAKE = "\ 
    -f '${S}/Makefile' \
    \
    'PREFIX=${prefix}' \
    'SBINDIR=${sbindir}' \
    'MANDIR=${mandir}' \
    'NLLIBNAME=libnl-tiny' \
    'NL1FOUND=""' \
    'NL2FOUND=Y' \
"

do_compile() {
    oe_runmake all
}

do_install() {
    oe_runmake 'DESTDIR=${D}' install
}
