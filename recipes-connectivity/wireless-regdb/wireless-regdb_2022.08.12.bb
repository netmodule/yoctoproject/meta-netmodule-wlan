SUMMARY = "Wireless Central Regulatory Domain Database"
HOMEPAGE = "http://wireless.kernel.org/en/developers/Regulatory/CRDA"
SECTION = "network"
LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://LICENSE;md5=07c4f6dea3845b02a18dc00c8c87699c"

DEPENDS = "openssl-native"

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/sforshee/wireless-regdb.git;protocol=http \
           file://100-regdb-write-firmware-file-format-version-code-20.patch \
           file://500-world-regd-5GHz.patch \
           file://501-change-python-interpreter-to-version-3-to-be-compat.patch \
           file://0001-ADD-regdb-EU-section-with-ranges-allowed-for-all-EU-.patch \
           "

SRCREV = "9dc9c89375c996c4d1d1a8c0a76f9dd04e244077"

S = "${WORKDIR}/git"

inherit pkgconfig allarch

do_compile () {
      oe_runmake maintainer-clean
      oe_runmake regulatory.db
}

do_install () {
     install -m 0644 -D regulatory.db ${D}${nonarch_base_libdir}/firmware/regulatory.db
}

# Install static regulatory DB in /lib/firmware for kernel to load.
# This requires Linux kernel >= v4.15.
# For kernel <= v4.14, inherit the kernel_wireless_regdb.bbclass in kernel's recipe.
PACKAGES =+ "${PN}-static"
RCONFLICTS_${PN} = "${PN}-static"

FILES_${PN}-static = " \
    ${nonarch_base_libdir}/firmware/regulatory.db \
"

RSUGGESTS_${PN} = "crda"
BBCLASSEXTEND = "native"
