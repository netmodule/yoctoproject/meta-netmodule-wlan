HOMEPAGE = "http://w1.fi/${PN}/"
BUGTRACKER = "http://w1.fi/security/"
SECTION = "network"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING;md5=5ebcb90236d1ad640558c3d3cd3035df \
                    file://README;beginline=1;endline=56;md5=e3d2f6c2948991e37c1ca4960de84747 \
                    file://wpa_supplicant/wpa_supplicant.c;beginline=1;endline=12;md5=76306a95306fee9a976b0ac1be70f705"
DEPENDS = "libnl-tiny"
PACKAGECONFIG ??= "gnutls"
PACKAGECONFIG[gnutls] = ",,gnutls libgcrypt"
PACKAGECONFIG[openssl] = ",,openssl"

inherit pkgconfig

PACKAGECONFIG = "openssl"

SRC_URI = "git://w1.fi/hostap.git;protocol=http;branch=main \
           file://wpa_supplicant-full.config \
           file://hostapd-full.config \
           file://src/utils/build_features.h \
           file://0001-Add-build-artifact-build_features.h-to-gitignore.patch \
           file://0002-Use-environment-variable-EXTRA_CFLAGS.patch \
           file://001-wolfssl-init-RNG-with-ECC-key.patch \
           file://010-mesh-Allow-DFS-channels-to-be-selected-if-dfs-is-ena.patch \
           file://011-mesh-use-deterministic-channel-on-channel-switch.patch \
           file://021-fix-sta-add-after-previous-connection.patch \
           file://022-hostapd-fix-use-of-uninitialized-stack-variables.patch \
           file://023-ndisc_snoop-call-dl_list_del-before-freeing-ipv6-add.patch \
           file://030-driver_nl80211-rewrite-neigh-code-to-not-depend-on-l.patch \
           file://040-mesh-allow-processing-authentication-frames-in-block.patch \
           file://050-build_fix.patch \
           file://100-daemonize_fix.patch \
           file://200-multicall.patch \
           file://300-noscan.patch \
           file://301-mesh-noscan.patch \
           file://310-rescan_immediately.patch \
           file://320-optional_rfkill.patch \
           file://330-nl80211_fix_set_freq.patch \
           file://340-reload_freq_change.patch \
           file://341-mesh-ctrl-iface-channel-switch.patch \
           file://350-nl80211_del_beacon_bss.patch \
           file://360-ctrl_iface_reload.patch \
           file://370-ap_sta_support.patch \
           file://380-disable_ctrl_iface_mib.patch \
           file://381-hostapd_cli_UNKNOWN-COMMAND.patch \
           file://390-wpa_ie_cap_workaround.patch \
           file://400-wps_single_auth_enc_type.patch \
           file://410-limit_debug_messages.patch \
           file://420-indicate-features.patch \
           file://430-hostapd_cli_ifdef.patch \
           file://431-wpa_cli_ifdef.patch \
           file://432-missing-typedef.patch \
           file://450-scan_wait.patch \
           file://460-wpa_supplicant-add-new-config-params-to-be-used-with.patch \
           file://461-driver_nl80211-use-new-parameters-during-ibss-join.patch \
           file://463-add-mcast_rate-to-11s.patch \
           file://464-fix-mesh-obss-check.patch \
           file://465-hostapd-config-support-random-BSS-color.patch \
           file://470-survey_data_fallback.patch \
           file://500-lto-jobserver-support.patch \
           file://590-rrm-wnm-statistics.patch \
           file://599-wpa_supplicant-fix-warnings.patch \
           file://610-hostapd_cli_ujail_permission.patch \
           file://700-wifi-reload.patch \
           file://710-vlan_no_bridge.patch \
           file://711-wds_bridge_force.patch \
           file://720-iface_max_num_sta.patch \
           file://730-ft_iface.patch \
           file://740-snoop_iface.patch \
           file://750-qos_map_set_without_interworking.patch \
           file://751-qos_map_ignore_when_unsupported.patch \
           file://910-wpa_supplicant_remove_acl.patch \
           file://912-iapp-integration.patch \
           file://913-iapp-improvements.patch \
           file://914-wlan-acs-srd-channels.patch \
           file://990-ctrl-make-WNM_AP-functions-dependant-on-CONFIG_AP.patch \
           file://991-fix-compile.patch \
           file://992-openssl-include-rsa.patch \
           file://0001-fix-create-a-versioned-shared-library-libwlan_client.patch \
           file://0001-use-nas-port-type-ethernet-for-wired-802.1X.patch \
           "

SRCREV = "4383528e01955d995d3b3db201e4c0f9840e8236"
SRC_URI[sha256sum] = "a1330574a241200188ff1478df25b58630012d7db91dfe092b8e0a3c50805ef0"

CVE_PRODUCT = "${TARGET_NAME}"
CONFFILES_${TARGET_NAME} += "${sysconfdir}/${TARGET_NAME}-full.config"

S = "${WORKDIR}/git"

export EXTRA_CFLAGS = " \
    ${CFLAGS} \
    -I${STAGING_DIR_TARGET}/usr/include/libnl-tiny \
    -lnl-tiny \
    -DCONFIG_LIBNL20 \
    -DCONFIG_LIBNL_TINY \
    -D_GNU_SOURCE \
"
LDFLAGS += " -Wl,--gc-sections"

EXTRA_OEMAKE_append = " WPAPV=${PV}.0"

export BINDIR = "${sbindir}"

do_configure () {
	${MAKE} -C ${TARGET_NAME} clean
        install -m 0755 ${WORKDIR}/${TARGET_NAME}-full.config ${TARGET_NAME}/.config
	install -m 0755 ${WORKDIR}/src/utils/build_features.h src/utils/build_features.h
	if echo "${PACKAGECONFIG}" | grep -qw "openssl"; then
        	ssl=openssl
	elif echo "${PACKAGECONFIG}" | grep -qw "gnutls"; then
        	ssl=gnutls
	fi
	if [ -n "$ssl" ]; then
        	sed -i "s/%ssl%/$ssl/" ${TARGET_NAME}/.config
	fi

	# For rebuild
	rm -f ${TARGET_NAME}/*.d ${TARGET_NAME}/dbus/*.d
}

do_compile () {
	unset CFLAGS CPPFLAGS CXXFLAGS
	oe_runmake -C ${TARGET_NAME}
}
