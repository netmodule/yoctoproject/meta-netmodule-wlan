SUMMARY = "Authenticator for Wi-Fi Protected Access (WPA)"

TARGET_NAME = "hostapd"

include hostapd.inc

RRECOMMENDS_${PN}_${PV} = "hostapd-cli"

PACKAGES_prepend = "hostapd-cli"
FILES_hostpad-cli = "${sbindir}/hostapd_cli"

do_install () {
	install -d ${D}${sbindir}
	install -m 755 hostapd/hostapd ${D}${sbindir}
	install -m 755 hostapd/hostapd_cli        ${D}${sbindir}
}
