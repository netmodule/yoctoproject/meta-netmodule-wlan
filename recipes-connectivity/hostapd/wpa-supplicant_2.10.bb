SUMMARY = "Client for Wi-Fi Protected Access (WPA)"

TARGET_NAME = "wpa_supplicant"
include hostapd.inc

RRECOMMENDS_${PN}_${PV} = "wpa_supplicant-passphrase wpa_supplicant-cli"


PACKAGES_prepend = "wpa_supplicant-passphrase wpa_supplicant-cli"
FILES_wpa_supplicant-passphrase = "${bindir}/wpa_passphrase"
FILES_wpa_supplicant-cli = "${sbindir}/wpa_cli"
FILES_${PN} += "${datadir}/dbus-1/system-services/*"

do_install () {
	install -d ${D}${sbindir}
	install -m 755 wpa_supplicant/wpa_supplicant ${D}${sbindir}
	install -m 755 wpa_supplicant/wpa_cli        ${D}${sbindir}
}

pkg_postinst_wpa_supplicant () {
	# If we're offline, we don't need to do this.
	if [ "x$D" = "x" ]; then
		killall -q -HUP dbus-daemon || true
	fi

}
