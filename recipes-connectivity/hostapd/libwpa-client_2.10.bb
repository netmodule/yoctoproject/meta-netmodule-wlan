SUMMARY = "A library for using the hostapd/wpa_supplicant control interface"

# This is part of wpa_supplicant and defined in the wpa_supplicant Makefile
TARGET_NAME = "wpa_supplicant"

include hostapd.inc

#PACKAGES_prepend = "libwpa_client"

FILES_${PN} += "${libdir}/*.so"
RPROVIDES_${PN} = "libwpa_client.so"
PROVIDES = "libwpa_client.so"


do_install () {
	install -d ${D}${libdir}
	install -d ${D}${includedir}/wpa_client
	
	install -m 755 wpa_supplicant/libwpa_client.so ${D}${libdir}/libwpa_client.so.2.10.0
	install -m 644 src/common/wpa_ctrl.h ${D}${includedir}/wpa_client
	ln -sf libwpa_client.so.2.10.0  ${D}${libdir}/libwpa_client.so 
}

do_compile () {
	unset CFLAGS CPPFLAGS CXXFLAGS
	oe_runmake -C ${TARGET_NAME} libwpa_client.so
}
