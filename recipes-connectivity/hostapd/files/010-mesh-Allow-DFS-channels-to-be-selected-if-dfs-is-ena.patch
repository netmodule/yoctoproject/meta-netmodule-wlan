From 4b6c9580dae47d20f429f0e2738183969acf29b8 Mon Sep 17 00:00:00 2001
From: Peter Oh <peter.oh@bowerswilkins.com>
Date: Tue, 30 Jun 2020 14:18:58 +0200
Subject: [PATCH] mesh: Allow DFS channels to be selected if dfs is enabled

Note: DFS is assumed to be usable if a country code has been set

Signed-off-by: Benjamin Berg <benjamin@sipsolutions.net>
Signed-off-by: Peter Oh <peter.oh@bowerswilkins.com>

---
 wpa_supplicant/wpa_supplicant.c | 31 ++++++++++++++++++++++---------
 1 file changed, 22 insertions(+), 9 deletions(-)

diff --git a/wpa_supplicant/wpa_supplicant.c b/wpa_supplicant/wpa_supplicant.c
index cf68417ca..fb47942c9 100644
--- a/wpa_supplicant/wpa_supplicant.c
+++ b/wpa_supplicant/wpa_supplicant.c
@@ -2425,7 +2425,7 @@ static int drv_supports_vht(struct wpa_supplicant *wpa_s,
 }
 
 
-static bool ibss_mesh_is_80mhz_avail(int channel, struct hostapd_hw_modes *mode)
+static bool ibss_mesh_is_80mhz_avail(int channel, struct hostapd_hw_modes *mode, bool dfs_enabled)
 {
 	int i;
 
@@ -2434,7 +2434,10 @@ static bool ibss_mesh_is_80mhz_avail(int channel, struct hostapd_hw_modes *mode)
 
 		chan = hw_get_channel_chan(mode, i, NULL);
 		if (!chan ||
-		    chan->flag & (HOSTAPD_CHAN_DISABLED | HOSTAPD_CHAN_NO_IR))
+		    chan->flag & HOSTAPD_CHAN_DISABLED)
+			return false;
+		
+		if (!dfs_enabled && chan->flag & (HOSTAPD_CHAN_RADAR | HOSTAPD_CHAN_NO_IR))
 			return false;
 	}
 
@@ -2463,6 +2466,8 @@ void ibss_mesh_setup_freq(struct wpa_supplicant *wpa_s,
 	int chwidth, seg0, seg1;
 	u32 vht_caps = 0;
 	bool is_24ghz, is_6ghz;
+	bool dfs_enabled = wpa_s->conf->country[0] &&
+			   (wpa_s->drv_flags & WPA_DRIVER_FLAGS_RADAR);
 
 	freq->freq = ssid->frequency;
 
@@ -2559,8 +2564,11 @@ void ibss_mesh_setup_freq(struct wpa_supplicant *wpa_s,
 		return;
 
 	/* Check primary channel flags */
-	if (pri_chan->flag & (HOSTAPD_CHAN_DISABLED | HOSTAPD_CHAN_NO_IR))
+	if (pri_chan->flag & HOSTAPD_CHAN_DISABLED)
 		return;
+	if (pri_chan->flag & (HOSTAPD_CHAN_RADAR | HOSTAPD_CHAN_NO_IR))
+		if (!dfs_enabled)
+			return;
 
 	freq->channel = pri_chan->chan;
 
@@ -2593,8 +2601,11 @@ void ibss_mesh_setup_freq(struct wpa_supplicant *wpa_s,
 		return;
 
 	/* Check secondary channel flags */
-	if (sec_chan->flag & (HOSTAPD_CHAN_DISABLED | HOSTAPD_CHAN_NO_IR))
+	if (sec_chan->flag & HOSTAPD_CHAN_DISABLED)
 		return;
+	if (sec_chan->flag & (HOSTAPD_CHAN_RADAR | HOSTAPD_CHAN_NO_IR))
+		if (!dfs_enabled)
+			return;
 
 	if (ht40 == -1) {
 		if (!(pri_chan->flag & HOSTAPD_CHAN_HT40MINUS))
@@ -2683,7 +2694,7 @@ skip_to_6ghz:
 		return;
 
 	/* Back to HT configuration if channel not usable */
-	if (!ibss_mesh_is_80mhz_avail(channel, mode))
+	if (!ibss_mesh_is_80mhz_avail(channel, mode, dfs_enabled))
 		return;
 
 	chwidth = CHANWIDTH_80MHZ;
@@ -2697,7 +2708,7 @@ skip_to_6ghz:
 		 * above; check the remaining four 20 MHz channels for the total
 		 * of 160 MHz bandwidth.
 		 */
-		if (!ibss_mesh_is_80mhz_avail(channel + 16, mode))
+		if (!ibss_mesh_is_80mhz_avail(channel + 16, mode, dfs_enabled))
 			return;
 
 		for (j = 0; j < ARRAY_SIZE(bw160); j++) {
@@ -2727,10 +2738,12 @@ skip_to_6ghz:
 				if (!chan)
 					continue;
 
-				if (chan->flag & (HOSTAPD_CHAN_DISABLED |
-						  HOSTAPD_CHAN_NO_IR |
-						  HOSTAPD_CHAN_RADAR))
+				if (chan->flag & HOSTAPD_CHAN_DISABLED)
 					continue;
+				if (chan->flag & (HOSTAPD_CHAN_RADAR |
+						  HOSTAPD_CHAN_NO_IR))
+					if (!dfs_enabled)
+						continue;
 
 				/* Found a suitable second segment for 80+80 */
 				chwidth = CHANWIDTH_80P80MHZ;
