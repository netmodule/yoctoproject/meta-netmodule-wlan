SUMMARY = "ath10k candelatech firmware for use with Linux kernel"
DESCRIPTION = "firmware for QCA ath10k based chipset like QCA988X from candelatech"
HOMEPAGE = "https://www.candelatech.com/"
SECTION = "base"
LICENSE = "CLOSED"

SRC_URI = "https://www.candelatech.com/downloads/firmware-2-ct-full-community-22.bin.lede.022"

SRC_URI[md5sum] = "3466a8f5e2edf8f737ce82d5aba71bd1"

S = "${WORKDIR}"

inherit allarch

do_compile() {
	:
}

do_install() {
	install -d ${D}${nonarch_base_libdir}/firmware/ath10k/QCA988X/hw2.0/
        cp  ${WORKDIR}/firmware-2-ct-full-community-22.bin.lede.022 ${D}${nonarch_base_libdir}/firmware/ath10k/QCA988X/hw2.0/firmware-2.bin
}



FILES_${PN} = "/lib/firmware"