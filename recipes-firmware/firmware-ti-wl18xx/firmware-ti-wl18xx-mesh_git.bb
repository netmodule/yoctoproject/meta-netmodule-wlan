SUMMARY = "TI wl18xx firmware for use with Linux kernel"
DESCRIPTION = "firmware forTI wl18xx based chipsets like wl1837"
HOMEPAGE = "https://git.ti.com/cgit/wilink8-wlan/wl18xx_fw/"
SECTION = "base"
LICENSE = "Firmware-ti-wl18xx"
LIC_FILES_CHKSUM = "file://LICENCE;md5=4977a0fe767ee17765ae63c435a32a9e"

NO_GENERIC_LICENSE[Firmware-ti-wl18xx]="LICENCE"

SRC_URI += " \
        git://git.ti.com/wilink8-wlan/wl18xx_fw.git \
        "

SRCREV = "5ec05007f2662f460f881c5868311fd3ab7e6e71"

S = "${WORKDIR}/git"

do_install () {
        install -d  ${D}${nonarch_base_libdir}/firmware/ti-connectivity/
        install -m 0644 wl18xx-fw-4.bin ${D}${nonarch_base_libdir}/firmware/ti-connectivity/wl18xx-fw-4-mesh.bin
}

FILES_${PN} = "${nonarch_base_libdir}/firmware"