SUMMARY = "ath10k firmware for use with Linux kernel"
DESCRIPTION = "firmware for QCA ath10k based chipset like QCA988X"
HOMEPAGE = "https://github.com/kvalo/ath10k-firmware.git"
SECTION = "base"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE.qca_firmware;md5=74852b14e2b35d8052226443d436a244"

RDEPENDS_${PN}-ath10k += "${PN}-ath10k-license"

PV = "1.0+git${SRCPV}"
SRCREV = "d622d160e9f552ead68d9ae81b715422892dc2ef"
SRC_URI = "git://github.com/kvalo/ath10k-firmware.git;protocol=https \
        "

SRC_URI[md5sum] = "60cda5c6fef6bae671aeb71696a81cff"

S = "${WORKDIR}/git"

inherit allarch

do_compile() {
	:
}

do_install() {
	install -d ${D}${nonarch_base_libdir}/firmware/ath10k/QCA988X/hw2.0/
        cp  QCA988X/hw2.0/board.bin ${D}${nonarch_base_libdir}/firmware/ath10k/QCA988X/hw2.0/.
        cp  QCA988X/hw2.0/10.2.4-1.0/firmware-5.bin_10.2.4-1.0-00047 ${D}${nonarch_base_libdir}/firmware/ath10k/QCA988X/hw2.0/firmware-5.bin
}

FILES_${PN}-atheros-license = "${D}${nonarch_base_libdir}/firmware/LICENSE.qca_firmware"
FILES_${PN} = "${nonarch_base_libdir}/firmware"

PACKAGES =+ "${PN}-ath10k-license ${PN}-ath10k"

# Firmware files are generally not ran on the CPU, so they can be
# allarch despite being architecture specific
INSANE_SKIP = "arch"
