SUMMARY = "TI wl18xx calibrator tool"
DESCRIPTION = "TI wl18xx calibrator tool"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING;md5=4725015cb0be7be389cf06deeae3683d"

SRC_URI = "git://git.ti.com/git/wilink8-wlan/18xx-ti-utils.git;protocol=http \
           file://wgod-ng \
           file://btgod-ng \
           file://0001-FIX-port-to-OE.patch \
           file://0001-Makefile-Make-buildable-from-yocto.patch \
           "

PV = "1.0+git${SRCPV}"
SRCREV = "87af888fe6f7b8fa5926a38a752f65451afe5f24"

S = "${WORKDIR}/git"

PACKAGECONFIG ?= "libnl-tiny"

PACKAGECONFIG[libnl] = ",,libnl,,"
PACKAGECONFIG[libnl-tiny] = ",,libnl-tiny,,"

# Special handling for libnl-tiny
OVERRIDES_append = ":${@bb.utils.contains("PACKAGECONFIG", "libnl-tiny", "libnl-tiny", "", d)}"

EXTRA_OEMAKE = "NLVER=3"
EXTRA_OEMAKE_libnl-tiny = "NLVER=2 LIBS="-lnl-tiny""

LIBNL_INCDIR = "libnl3"
LIBNL_INCDIR_libnl-tiny = "libnl-tiny"

CFLAGS += "-I${STAGING_INCDIR}/${LIBNL_INCDIR} \
           -D_GNU_SOURCE \
"

do_install () {
    install -d ${D}${bindir}
    install -m 755 ${S}/calibrator ${D}${bindir}/.
    install -m 755 ${WORKDIR}/wgod-ng ${D}${bindir}/.
    install -m 755 ${WORKDIR}/btgod-ng ${D}${bindir}/.
}

FILES_${PN} += "${bindir}/*"
