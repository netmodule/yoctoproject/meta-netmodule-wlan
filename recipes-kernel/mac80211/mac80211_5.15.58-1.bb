# Copyright (C) 2017 Khem Raj <raj.khem@gmail.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "Linux Backports"
HOMEPAGE = "https://backports.wiki.kernel.org"
SECTION = "kernel/modules"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

inherit module cml1

include nrsw-modules.${NM_TARGET}.inc

SHRT_VER = "${@d.getVar('PV', True).split('-')[0]}"

SRC_URI = "http://www.kernel.org/pub/linux/kernel/projects/backports/stable/v${SHRT_VER}/backports-${PV}.tar.gz \
           file://config.${NM_TARGET} \
           file://0001-backport-of-build-patches-from-openwrt.patch \
           file://0002-backport-of-subsys-patches-from-openwrt.patch \
           file://0003-backport-of-ath-patches-from-openwrt.patch \
           file://0004-backport-of-ath5k-patches-from-openwrt.patch \
           file://0005-backport-of-ath9k-patches-from-openwrt.patch \
           file://0006-backport-of-ath10k-patches-from-openwrt.patch \
           file://0007-backport-of-brcm-patches-from-openwrt.patch \
           file://0008-backport-of-mwl-patches-from-openwrt.patch \
           file://0009-backport-of-rt2x00-patches-from-openwrt.patch \
           file://0010-netmodule-patches.patch \
           "

SRC_URI[sha256sum] = "4c6b2af699e5e557dfc44bc7e30a10f1d6299a451ea50443084bdf7c850cbb24"


S = "${WORKDIR}/backports-${PV}"

DEPENDS += "coreutils-native flex-native bison-native"

EXTRA_OEMAKE = "KLIB_BUILD=${STAGING_KERNEL_BUILDDIR} KLIB=${base_libdir}/modules/${KERNEL_VERSION}"
KCONFIG_CONFIG_COMMAND = "${EXTRA_OEMAKE} CC=${BUILD_CC} menuconfig"

MAKE_TARGETS = "modules"

PACKAGE_CONFIGFILE := "config.${NM_TARGET}"

do_configure_prepend() {
	# Make sure kconf is built to run on the host
	unset CFLAGS CPPFLAGS CXXFLAGS LDFLAGS
	oe_runmake -C kconf CC=${BUILD_CC} conf
        # Copy our config as a defconfig

	if [ ! -n "${PACKAGE_CONFIGFILE}" ]; then
           bbfatal "mac80211: no config file given"
        fi

        cp ${WORKDIR}/${PACKAGE_CONFIGFILE} ${S}/.config
	oe_runmake oldconfig

}

# Create a meta package with another name, in this way we can
# always build this receipe but only install the modules
# when requiered.
KERNEL_MODULES_META_PACKAGE = "kernel-module-wlan-backports"

FILES_${KERNEL_MODULES_META_PACKAGE} = ""
ALLOW_EMPTY_${KERNEL_MODULES_META_PACKAGE} = "1"

PACKAGES += "${KERNEL_MODULES_META_PACKAGE}"

PACKAGES_DYNAMIC += "^${KERNEL_PACKAGE_NAME}-module-.*"

KERNEL_MODULE_AUTOLOAD += "\
        compat \
        mac80211 \
        cfg80211 \
        "


